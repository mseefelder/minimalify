#!/bin/bash

g++ main.cpp -std=c++11 -I../ann/include -L../ann/lib -lANN -lX11 -pthread -o minimalify

g++ -g main.cpp -std=c++11 -I../ann/include -L../ann/lib -lANN -lX11 -pthread -o debug-minimalify