#include <cmath>
#include <cstdio>
#include <queue>
#include <algorithm>
#include <ANN/ANN.h>
#define DIM 5
#ifndef LOOPS
	#define LOOPS 1000
#endif

inline void RGBtoLUV(double& r, double& g, double& b);

inline double dist2color(ANNpoint p, ANNpointArray a, ANNidx& i) {
	return 	(a[i][2]-p[2])*(a[i][2]-p[2]) + 
			(a[i][3]-p[3])*(a[i][3]-p[3]) + 
			(a[i][4]-p[4])*(a[i][4]-p[4]);
}

inline double dist2space(ANNpoint p, ANNpointArray a, ANNidx& i) {
	return  (a[i][0]-p[0])*(a[i][0]-p[0]) +
			(a[i][1]-p[1])*(a[i][1]-p[1]);
}

void meanshift (double* input, double* filter, double* segment, int width, int height, int neigs, double hs, double hr) {
	//Limit to stop iterations
	const double limit = 0.01;
	//Number of samples in the image
	const int s = width*height;

	//Create point Array to store samples
	ANNpointArray samples = annAllocPts(s, DIM);
	//Create point Array to store kernels
	ANNpointArray kernels = annAllocPts(s, DIM);

	//Set sample values, converting RGB to Luv
	int i = 0; //temp index
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			i = x + y*width;
			
			samples[i][0] = (double) x;
			samples[i][1] = (double) y;
			samples[i][2] = input[i];
			samples[i][3] = input[i+s];
			samples[i][4] = input[i+s+s];
			//convert colors
			RGBtoLUV(samples[i][2], samples[i][3], samples[i][4]);

			//kernels, at first, are identical to samples
			kernels[i][0] = samples[i][0];
			kernels[i][1] = samples[i][1];
			kernels[i][2] = samples[i][2];
			kernels[i][3] = samples[i][3];
			kernels[i][4] = samples[i][4];
		}	
	}

	//Create kd Tree
	ANNkd_tree kdTree(samples, s, DIM);
	//Array for neighbor indices and distances
	ANNidxArray nIdx = new ANNidx[neigs];
	ANNdistArray nDis = new ANNdist[neigs];

	//kernel and neighbor coordinates
	double kx, ky, kl, ku, kv, nx, ny, nl, nu, nv;
	//update coordinates
	double ux, uy, ul, uu, uv;
	//weights
	double weightSpace, weightColor, currentWeight, totalWeight;
	//length and variables to check if iteration must be over
	double length = 0.0;
	bool iterate = true;
	int loopCount = 0;
	long meanIter = 0;
	int maxIter = 0;

	/**/
	//For each kernel, run meanshift until convergence
	std::cout<<" "<<std::endl;
	for (int k = 0; k < s; ++k) {
//		std::cout<<"\r"<<(double)k/(double)s<<" %"<<std::endl;
		std::printf("\r %f ", (double)k/(double)s);
		std::fflush(stdout);
		iterate = true;
		loopCount = 0;
		while(iterate && (loopCount < LOOPS)) {
			//Set point:
			// spatial part
			kx = kernels[k][0]; ky = kernels[k][1];
			// color part
			kl = kernels[k][2]; ku = kernels[k][3]; kv = kernels[k][4];

			//Get neigs
			kdTree.annkSearch(kernels[k], neigs, nIdx, nDis);

			//Zero-out update coordinates and total weight
			ux = 0; uy = 0; ul = 0; uu = 0; uv = 0;
			totalWeight = 0;

			//Accumulate points in U and total weight in totalWeight
			for (int n = 0; n < neigs; ++n) {
				//Set neighbor:
					// spatial part
					nx = samples[nIdx[n]][0]; ny = samples[nIdx[n]][1];
					// color part
					nl = samples[nIdx[n]][2]; nu = samples[nIdx[n]][3]; 
					nv = samples[nIdx[n]][4];

				//Calculate weights...
				//...spatial
				weightSpace = (nx-kx)*(nx-kx) + (ny-ky)*(ny-ky);
				weightSpace = weightSpace / (hs*hs);
				weightSpace = std::exp(-0.5*weightSpace);
				//...and color
				weightColor = (nl-kl)*(nl-kl) + (nu-ku)*(nu-ku) + (nv-kv)*(nv-kv);
				weightColor = weightColor / (hr*hr);
				weightColor = std::exp(-0.5*weightColor);
				//multiply them
				currentWeight = weightSpace*weightColor;
				//accumulate update coordinates...
				ux += currentWeight*nx;
				uy += currentWeight*ny;
				ul += currentWeight*nl;
				uu += currentWeight*nu;
				uv += currentWeight*nv;
				//... and total weight
				totalWeight += currentWeight;
			}
			//normalize update coordinates with total weight
			ux = ux/totalWeight;
			uy = uy/totalWeight;
			ul = ul/totalWeight;
			uu = uu/totalWeight;
			uv = uv/totalWeight;

			//calculate shift vector length
			length = (ux-kx)*(ux-kx)+(uy-ky)*(uy-ky)+
					 (ul-kl)*(ul-kl)+(uu-ku)*(uu-ku)+(uv-kv)*(uv-kv);
			length = std::sqrt(length);
			//Check if has to iterate again
			iterate = (length > limit);

			//Copy updated kernel to kernel array:
			// spatial part
			kernels[k][0] = ux; kernels[k][1] = uy;
			// color part
			kernels[k][2] = ul; kernels[k][3] = uu; kernels[k][4] = uv;

			//Increase loop count
			loopCount++;
		}

		meanIter = (k*meanIter + loopCount)/(k+1);
		maxIter = std::max(maxIter, loopCount);
	}

	std::cout<<"\n Mean # of loops: "<<meanIter<<std::endl;
	std::cout<<"\n Max # of loops: "<<maxIter<<std::endl;
	/**/


	/*>>====================:FILTERING OUTPUT:====================<<*/
	//Get final colors from filtering
	i = 0;
	for (int k = 0; k < s; ++k) {
		i = (int)kernels[k][0] + ((int)kernels[k][1])*(width);
		
		filter[k]     = input[i];
		filter[k+s]   = input[i+s];
		filter[k+s+s] = input[i+s+s];
	}

	/*>>====================:CLUSTERING BFS:====================<<*/
	
	//Set up kd tree with kernels
	ANNkd_tree clusterTree(kernels, s, DIM);
	//Auxiliary queue used during the algorithm
	std::queue<int> fifo;
	//For each vertex: store its parent
	int* clusters = new int[s];
	int current = 0;
	int clusterId = 0;
	//Fill arrays
	for (int k = 0; k<s; k++){
	  clusters[k]=-1;
	}

	int k = 0;
	int n = 0;
	for (int y = 0; y < height; ++y) {
		for (int x = 0; x < width; ++x) {
			k = x + y*width;
			std::printf("\r %f ", (double)k/(double)s);
			std::fflush(stdout);

			//If already belongs to cluster, skip
			if (clusters[k]>=0) {
				continue;
			}
			//Add kernel to new cluster
			clusters[k] = clusterId;

			//Add kernel to BFS FIFO
			fifo.push(k);

			while(!fifo.empty()){
				current = fifo.front();
				for (int ny = -1; ny < 2; ++ny) {
					for (int nx = -1; nx < 2; ++nx) {
						n = current + nx + (ny*width);
						if (n<0 || n>=s || n==current) continue;
						if 
						(
							clusters[n] < 0 
							&&
							dist2color(kernels[current], kernels, n) < hr*hr
						){
							fifo.push(n);
							clusters[n] = clusterId;
						}
					}
				}
				fifo.pop();
			}

			clusterId++;
		}	
	}


	std::cout<<"\n"<<clusterId<<" clusters."<<std::endl;

	double* clusterX = new double[clusterId];
	double* clusterY = new double[clusterId];
	double* clusterR = new double[clusterId];
	double* clusterG = new double[clusterId];
	double* clusterB = new double[clusterId];
	double* clusterCount = new double[clusterId];

	for (int i = 0; i < clusterId; ++i) {
		clusterX[i] = 0.0;
		clusterY[i] = 0.0;
		clusterCount[i] = 0.0;
	}

	int currentCluster = 0;
	double divider = 0.0;
	std::cout<<" "<<std::endl;
	for (int k = 0; k < s; ++k) {
		std::printf("\r %f ", (double)k/(double)s);
		std::fflush(stdout);
		currentCluster = clusters[k];

		clusterX[currentCluster] = 
			clusterCount[currentCluster]*clusterX[currentCluster] + kernels[k][0];
		clusterY[currentCluster] = 
			clusterCount[currentCluster]*clusterY[currentCluster] + kernels[k][1];
		
		clusterR[currentCluster] = 
			clusterCount[currentCluster]*clusterR[currentCluster] + filter[k];
		clusterG[currentCluster] = 
			clusterCount[currentCluster]*clusterG[currentCluster] + filter[k+s];
		clusterB[currentCluster] = 
			clusterCount[currentCluster]*clusterB[currentCluster] + filter[k+s+s];
		
		clusterCount[currentCluster] += 1.0;

		clusterX[currentCluster] /= clusterCount[currentCluster];
		clusterY[currentCluster] /= clusterCount[currentCluster];
		clusterR[currentCluster] /= clusterCount[currentCluster];
		clusterG[currentCluster] /= clusterCount[currentCluster];
		clusterB[currentCluster] /= clusterCount[currentCluster];
	}

	int colorindex = 0;
	for (int k = 0; k < s; ++k) {
		currentCluster = clusters[k];
		segment[k]     = clusterR[currentCluster];
		segment[k+s]   = clusterG[currentCluster];
		segment[k+s+s] = clusterB[currentCluster];
	}

	delete [] clusterX;
	delete [] clusterY;
	delete [] clusterCount;
	delete [] clusters;

	//Deallocate stuff
	annDeallocPts(samples);
	delete [] nIdx;
	delete [] nDis;
	annClose();

	std::cout<<std::endl;
}

inline void RGBtoLUV(double& r, double& g, double& b) {
	double x, y, z, denominator;
	r /= 255.0;
	g /= 255.0;
	b /= 255.0;

	if ( r > 0.04045 ) 
		r = std::pow( ( ( r + 0.055 ) / 1.055 ), 2.4);
	else                   
		r = r / 12.92;
	
	if ( g > 0.04045 ) 
		g = std::pow( ( ( g + 0.055 ) / 1.055 ), 2.4);
	else                   
		g = g / 12.92;
	
	if ( b > 0.04045 ) 
		b = std::pow( ( ( b + 0.055 ) / 1.055 ), 2.4);
	else                   
		b = b / 12.92;

	x = 0.412453*r + 0.357580*g + 0.180423*b;
	y = 0.212671*r + 0.715160*g + 0.072169*b;
	z = 0.019334*r + 0.119193*g + 0.950227*b;

	r = y;
	r = (r > 0.008856) ? 116.0 * std::pow(r, 1.0/3.0) - 16.0 : 903.3 * r;

	denominator = x+15.0*y+3.0*z;

	g = (denominator == 0) ? 4.0 : (4.0*x)/denominator;
	b = (denominator == 0) ? 9.0/15.0 : (9.0*y)/denominator;	

	g = 13.0*r*(g - 0.19784977571475);
	b = 13.0*r*(b - 0.46834507665248);
}