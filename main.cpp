#include <string>
#include <iostream>
#include "../CImg/CImg.h"
#include "meanshift.hpp"

int main(int argc, char const *argv[])
{
	cimg_library::CImg<double> img1;

	if (argc < 5) { // Check the value of argc. If not enough parameters have been passed, inform user and exit.
		std::cout << "Usage is:\n"<<
			argv[0]<<" <image path> <k neigs> <hs> <hr>\n"<<
			"Press any key to continue..."; // Inform the user of how to use the program
		std::cin.get();
		return 0;
	} else {
		//try and open both files
		try {
			std::cout<<"Opening files..."<<std::endl;
			img1.assign(argv[1]);
		} catch (int e) {
			std::cout << "Unable to open files. Use -h for help\n";
			std::cin.get();
			return 0;
		}
	}

	std::cout<<"Files opened..."<<std::endl;

	long size = img1.width()*img1.height()*img1.spectrum();
	double* A = new double[size];
	double* B = new double[size];
	std::cout<<"Running meanshift..."<<std::endl;
	meanshift(img1.data(), A, B, img1.width(), img1.height(), std::stoi(argv[2]), (double) std::stoi(argv[3]), (double) std::stoi(argv[4]));

	//double* output = pingpong ? A : B;
	cimg_library::CImg<double> imgFilter(A, img1.width(), img1.height(), 1, 3);
	cimg_library::CImg<double> imgSegment(B, img1.width(), img1.height(), 1, 3);
	imgFilter.save_png("filter.png");
	imgSegment.save_png("segment.png");
	cimg_library::CImgDisplay img_disp(imgSegment, "Minimalify");
		while (!img_disp.is_closed()) { 
	}

	delete [] A;

	return 0;
}