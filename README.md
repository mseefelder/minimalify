# Minimalify (Meanshift Segmentation)

## Dependencies

This work depends on:

* [CImg.h](http://cimg.eu/);
* [ANN](https://www.cs.umd.edu/~mount/ANN/);

CImg.h is supposed to be in `../CImg/` in relation to the project root.
To chenge it, edit the `#include` line in *main.cpp*.

The path to ANN can be read on *compile.sh*. 
The library root is supposed to be in `../ann/` in relation to the project root.

## Compiling

If the paths are correct, run *compile.sh*. If not, edit it and/or *main.cpp*.

## Running

Instructions are given when calling the application with no parameters.